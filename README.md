# Emmy - Web Server

This is the web server for [Emmy](https://gitlab.com/xujunfeng/emmy).

## Setting Up

1. Make sure that your machine has [Node.js](https://nodejs.org/en/)
   and [npm](https://www.npmjs.com/) installed.
   
2. [Install Emmy](https://gitlab.com/xujunfeng/emmy#how-to-install-emmy).

3. Run the following commands:

```
git clone https://gitlab.com/xujunfeng/emmy-server.git
cd emmy-server
npm install
```

4. Set up the following environment variables:
   - `EMMY_SERVER_COMMAND`**(Required)**: the command to run Emmy.
     If you have installed Emmy already, then
     ```
     racket -l emmy -- --json <Arguments>
     ```
     would be fine.
     ([More about running Emmy here](https://gitlab.com/xujunfeng/emmy#running-emmy))
     
   - `EMMY_SERVER_WORKING_PORT`(Optional): the port the server listens to.
      The default port is `3001`.
     
   - `EMMY_SERVER_WORKING_DIRECTORY`(Optional): the working directory in which
      we call Emmy.
      If `EMMY_SERVER_WORKING_DIRECTORY` is not set, then Emmy will be called
      in the current directory.
     
5. Run `npm start` to start the server on localhost.
