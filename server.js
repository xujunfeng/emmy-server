require('dotenv').config()

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const path = require('path')
const child_process = require('child_process')

const app = express()

const dir_var = process.env.EMMY_SERVER_WORKING_DIRECTORY;
const command_var = process.env.EMMY_SERVER_COMMAND;

if (command_var === undefined) {
  throw new Error("Command is undefined");
}

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json())
app.use(cors());

app.post('/', (req, res, next) => {
  let dir;
  let input;
  let out;
  if (dir_var !== undefined) {
    try {
      dir = path.resolve(
        process.cwd(),
        process.env.EMMY_SERVER_WORKING_DIRECTORY
      );
    } catch (err) {
      next(err);
    }
  }
  try {
    input = req.body.program.map(JSON.stringify).join("\n");
  } catch (err) {
    next(err);
  }
  try {
    out = child_process.execSync(
      process.env.EMMY_SERVER_COMMAND,
      {
        cwd: dir,
        input: input,
      }
    );
  } catch (err) {
    console.error("Proof checker crashed.");
    console.error("Stderr was: " + err.stderr);
    console.error("Stdout was: " + err.stderr);
    next(err);
  }

  try {
    res.json(JSON.parse(out.toString()));
  } catch (err) {
    console.error("Error when responding. Output was: " + out.toString());
    next(err);
  }

});

app.listen(process.env.EMMY_SERVER_PORT || 3001);
